package blockchainproject;
import java.util.ArrayList;
import java.util.Date;

public class block {
    // This is the class that represents a block in the blockchain.
    // There are the following attributes to the block, each initialized when 
    // the block is instantiated. 
    public String hash;
    public String lastHash;
    public String transactionHash;
    public ArrayList<transaction> transactions = new ArrayList<transaction>();
    public long timestamp; 
    public int nonce;

    // Instantiate a new block and set values for each attribute.
    public block(String lastHash) {
        this.lastHash = lastHash;
        this.timestamp = new Date().getTime();
        this.hash = getHash();
    }

    // Calculate the hash for a given block.
    public String getHash() {
        String hashedHash = stringHash.hashSha512(Integer.toString(nonce) + lastHash + Long.toString(timestamp) + transactionHash);
        return hashedHash;
    }

    // Proof of work calculation to mine a block with a given number of 0s
    public void mineBlock() {
        // Set the target value for the string
        String target = "000";
        transactionHash = stringHash.getMerkleDigest(transactions);
        while ( true ) {
            // Stop if it finds a valid hash
            if (hash.substring(0,target.length()).equals(target)) {
                break;
            }
            nonce++;
            /*
            // For debugging only
            //System.out.println("Hash: " + hash);
            //System.out.println("target: " + target);
            */
            hash = getHash();
        }
        //System.out.println("Hash: " + hash);
    }

    public boolean doATransact (transaction transaction) {
        if (transaction == null) {
            return false;
        }
        if ((lastHash != "0")) {
            if ((transaction.doATransaction() != true)) {
                System.out.println("Transaction failed");
                return false;
            }
        }
        transactions.add(transaction);
        System.out.println("Transaction successful");
        return true;
    }

}
