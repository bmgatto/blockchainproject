package blockchainproject;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class wallet {
    public PrivateKey privatekey;
    public PublicKey publickey;

    public HashMap<String, transactionOutput> UTXOs = new HashMap<String, transactionOutput>();  //only UTXOs owned by this wallet.


    public wallet() {
        generateKeyPair();
    }

    public void generateKeyPair() {
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("ECDSA", "BC");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            ECGenParameterSpec ecgen = new ECGenParameterSpec("prime192v1");
            keygen.initialize(ecgen, random);
            KeyPair keypair = keygen.generateKeyPair();

            publickey = keypair.getPublic();
            privatekey = keypair.getPrivate();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
  //returns balance and stores the UTXO's owned by this wallet in this.UTXOs
    public float getWallet() {
        float total = 0;
        for (Map.Entry<String, transactionOutput> item: blockchain.UTXOs.entrySet()) {
            transactionOutput UTXO = item.getValue();
            if (UTXO.checkCoin(publickey)) {
                UTXOs.put(UTXO.ID, UTXO);   //add it to our list of unspent transactions.
                total += UTXO.value;
            }
        }
        return total;
    }
    //Generates and returns a new transaction from this wallet.
	public transaction transferCoins(PublicKey reciever, float value) {
		if(getWallet() < value) {
			System.out.println("No monies");
			return null;
		}
		//create array list of inputs
		ArrayList<transactionInput> inputs = new ArrayList<transactionInput>();
    
		float total = 0;
		for (Map.Entry<String, transactionOutput> item: UTXOs.entrySet()){
			transactionOutput UTXO = item.getValue();
			total += UTXO.value;
			inputs.add(new transactionInput(UTXO.ID));
			if(total > value) break;
		}
		
		transaction newTransaction = new transaction(publickey, reciever, value, inputs);
		newTransaction.createSignature(privatekey);
		
		for(transactionInput input: inputs){
			UTXOs.remove(input.transactionOutputID);
		}
		return newTransaction;
	}
	
}
