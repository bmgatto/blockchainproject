package blockchainproject;
import java.security.MessageDigest;
import java.security.*;
import java.util.ArrayList;
import java.util.Base64;

// Really fancy method to find the hash of the given string.
// Lovingly referenced from https://www.baeldung.com/sha-256-hashing-java
public class stringHash {
    public static String hashSha512(String input) {
        try {
            // Create the message digest.
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            // Get the hex value of the hash digest.
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            // Return the hex.
            return hexString.toString();
        }
        catch (Exception e) {
            // Just in case.
            // Also, it wouldn't let me do it without this...
            throw new RuntimeException(e);
        }
    }

    public static byte[] createECDSA(PrivateKey privatekey, String input) {
        Signature dsa;
        byte[] output = new byte[0];
        try {
            dsa = Signature.getInstance("ECDSA", "BC");
            dsa.initSign(privatekey);
            byte[] bytes = input.getBytes();
            dsa.update(bytes);
            byte[] signedsignature = dsa.sign();
            output = signedsignature;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return output;
    }

    public static boolean checkECDSA(PublicKey publickey, String data, byte[] signature) {
        try {
            Signature checker = Signature.getInstance("ECDSA", "BC");
            checker.initVerify(publickey);
            checker.update(data.getBytes());
            return checker.verify(signature);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getStringFromKey(Key key) {
        return Base64.getEncoder().encodeToString(key.getEncoded());
    }

    // Due to the massive size of transactions in any given block, 
    // this function will allow for any number of transactions to be 
    // compiled into a single hash that we can work with. 
    public static String getMerkleDigest(ArrayList<transaction> transactions) {
        int count = transactions.size();
        ArrayList<String> lastLayer = new ArrayList<String>();
        for (transaction transaction : transactions) {
            lastLayer.add(transaction.transactionID);
        }
        ArrayList<String> thisLayer = lastLayer;
        while (count > 1) {
            thisLayer = new ArrayList<String>();
            for (int i = 1; i < lastLayer.size(); i++) {
                thisLayer.add(hashSha512(lastLayer.get(i-1) + lastLayer.get(i)));
            }
            count = thisLayer.size();
            lastLayer = thisLayer;
        }
        String topHash = (thisLayer.size() == 1) ? thisLayer.get(0) : "";
        return topHash;
    }

}
