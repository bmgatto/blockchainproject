package blockchainproject;
import java.security.PublicKey;

public class transactionOutput {
	public String ID;
	public PublicKey reciever; //also known as the new owner of these coins.
	public float value; //the amount of coins they own
	public String parentTransactionID; //the id of the transaction this output was created in
	
	//Constructor
	public transactionOutput(PublicKey reciever, float value, String parentTransactionID) {
		this.reciever= reciever;
		this.value = value;
		this.parentTransactionID = parentTransactionID;
		this.ID = stringHash.hashSha512(stringHash.getStringFromKey(reciever)+Float.toString(value)+parentTransactionID);
	}
	
	//Check if coin belongs to you
	public boolean checkCoin(PublicKey publicKey) {
		return (publicKey == reciever);
	}
	
}
