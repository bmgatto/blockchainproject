package blockchainproject;

public class transactionInput {
	public String transactionOutputID; //Reference to TransactionOutputs -> transactionId
	public transactionOutput UTXO;     //Contains the Unspent transaction output
	
	public transactionInput(String transactionOutputID) {
		this.transactionOutputID = transactionOutputID;
	}
}
