package blockchainproject;
import org.bouncycastle.*;
import java.security.Security;
import java.util.Base64;
import java.util.ArrayList;
import java.util.HashMap;

public class blockchain {

    // This arraylist will hold the blocks in a... well, blockchain
    public static ArrayList<block> chainedBlocks = new ArrayList<block>();
	public static HashMap<String, transactionOutput> UTXOs = new HashMap<String, transactionOutput>();
    public static float transactionMinimum = 0.1f;
    public static wallet userWallet;
    public static wallet shutterBlock;
    public static transaction genesisTransaction;

    // Main method
    public static void main(String[] args) {
        /*
        // Creates a genesis block with a transaction and a starting prev hash.
        block genesisBlock = new block ("This is the first transaction", "0000");
        // Chains the blocks together.
        chainedBlocks.add(genesisBlock);
        // Here it does the fun proof of work thing to find the hash that matches the 
        // target pattern set in the other class file.
        chainedBlocks.get(0).mineBlock();
        System.out.println("Block 0: " + genesisBlock.hash);

        // Same as before, but references the hash of the genesis block instead.
        block firstBlock = new block("This is the second transaction", genesisBlock.hash);
        chainedBlocks.add(firstBlock);
        chainedBlocks.get(1).mineBlock();
        System.out.println("Block 1: " + firstBlock.hash);
        
        block secondBlock = new block("This is the third transaction", firstBlock.hash);
        chainedBlocks.add(secondBlock);
        chainedBlocks.get(2).mineBlock();
        System.out.println("Block 2: " + secondBlock.hash);

        System.out.println("Blockchain is valid: " + chainChecker());
        */
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); 

        userWallet = new wallet();
        shutterBlock = new wallet();
        wallet coins = new wallet();

        genesisTransaction = new transaction(coins.publickey, userWallet.publickey, 100f, null);
        genesisTransaction.createSignature(coins.privatekey);
        genesisTransaction.transactionID = "0";
        genesisTransaction.outputs.add(new transactionOutput(genesisTransaction.reciever, genesisTransaction.value, genesisTransaction.transactionID));
        UTXOs.put(genesisTransaction.outputs.get(0).ID, genesisTransaction.outputs.get(0));

		System.out.println("Creating genesis block");
		block genesis = new block("0");
		genesis.doATransact(genesisTransaction);
        doABlock(genesis);



        System.out.println("\n");
        block firstBlock = new block(genesis.hash);
        System.out.println("User has: " + userWallet.getWallet());
        System.out.println("User has spent: " + shutterBlock.getWallet());
        
        System.out.println("\n");
        System.out.println("User spends 20");
        firstBlock.doATransact(userWallet.transferCoins(shutterBlock.publickey, 20f));
        doABlock(firstBlock);
        System.out.println("\n");
        
        System.out.println("User has: " + userWallet.getWallet());
        System.out.println("User has spent: " + shutterBlock.getWallet());


        System.out.println("\n");
        System.out.println("Chain is valid: " + chainChecker());

    }

    // Method to check the validity of the chain.
    // Makes sure that every block has a valid hash and references the previous block correctly.
    public static Boolean chainChecker() {
        block thisBlock; 
        block lastBlock;

        // Very simple, just gets the block and the last block and makes sure that the hash is right
        // and the last hash is valid.
        for (int i = 1; i < chainedBlocks.size(); i++) {
            thisBlock = chainedBlocks.get(i);
            lastBlock = chainedBlocks.get(i-1);

            // If the current block is what it should then it's good.
            if ( thisBlock.hash.equals(thisBlock.getHash()) ) {
                // For debugging purposes.
                //System.out.println("Current hashes are equal");
                assert true;
            } else {
                //System.out.println("Current hashes are NOT equal");
                return false;
            }

            // Likewise with the previous hash.
            if ( lastBlock.hash.equals(thisBlock.lastHash) ) {
                // For debugging purposes.
                //System.out.println("Previous hashes are equal");
                assert true;
            } else {
                //System.out.println("Previous hashes are NOT equal");
                return false;
            }
        }
        return true;
    }
    
    public static void doABlock(block didBlock) {
		didBlock.mineBlock();
		chainedBlocks.add(didBlock);
    }

}
