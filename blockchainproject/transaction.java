package blockchainproject;
import java.security.*;
import java.util.ArrayList;

public class transaction {

    public String transactionID; //This is also the hash of the transaction.
    public PublicKey sender;     //sender address/public key.
    public PublicKey reciever;   //Recipients address/public key.
    public float value;
    public byte[] signature;     //This is to prevent anybody else from spending funds in our wallet.
    private static int sequence = 0; // A rough count of how many transactions have been generated.

    public ArrayList<transactionInput> inputs = new ArrayList<transactionInput>();
	public ArrayList<transactionOutput> outputs = new ArrayList<transactionOutput>();

    public transaction(PublicKey sender, PublicKey reciever, float value, ArrayList<transactionInput> inputs) {
        this.sender = sender;
        this.reciever = reciever;
        this.value = value;
        this.inputs = inputs;
    }
 // This Calculates the transaction hash (which will be used as its Id)
	private String getHash() {
        sequence++;
        String sender2 = stringHash.getStringFromKey(sender);
        String reciever2 = stringHash.getStringFromKey(reciever);
        String value2 = Float.toString(value);
        return stringHash.hashSha512(sequence + sender2 + reciever2 + value2);
    }

    public void createSignature(PrivateKey privatekey) {
        String data = stringHash.getStringFromKey(sender) + stringHash.getStringFromKey(reciever) + Float.toString(value);
        signature = stringHash.createECDSA(privatekey, data);
    }

    public boolean checkSignature() {
        String data = stringHash.getStringFromKey(sender) + stringHash.getStringFromKey(reciever) + Float.toString(value);
        return stringHash.checkECDSA(sender, data, signature);
    }
    //Returns true if new transaction could be created.	
    public boolean doATransaction() {
        if (checkSignature() == false) {
            System.out.println("Transaction failed");
            return false;
        }
        //gather transaction inputs (Make sure they are unspent):
        for (transactionInput i : inputs) {
            i.UTXO = blockchain.UTXOs.get(i.transactionOutputID);
        }
        //generate transaction outputs:
        float remainder = getValueInput() - value;
        transactionID = getHash();
        outputs.add(new transactionOutput(this.reciever, value, transactionID));
        outputs.add(new transactionOutput(this.sender, remainder, transactionID));
      //add outputs to Unspent list
        for (transactionOutput j : outputs) {
            blockchain.UTXOs.put(j.ID, j);
        }
        //remove transaction inputs from UTXO lists as spent:
        for (transactionInput k : inputs) {
            blockchain.UTXOs.remove(k.UTXO.ID);
        }
        
        return true;
    }
       //returns sum of inputs(UTXOs) values
    public float getValueInput () {
        float total = 0;
        for (transactionInput i : inputs) {
            total += i.UTXO.value;
        }
        return total;
    }
     //returns sum of outputs:
    public float getValueOutput () {
        float total = 0;
        for (transactionOutput i : outputs) {
            total += i.value;
        }
        return total;
    }

}
